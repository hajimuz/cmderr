---
title: "About CmdErr"
date: 2020-09-23T16:50:44+08:00
draft: false
---

## About CmdErr

CmdErr is a short form for **Command Error**, a term that every system administration or developer should be familiar with.
CmdErr.com is going to be a blog generally about topics including but not limited to System Maintenance and Development, Openstack, Kubernetes/Openshift, Ceph, Web development etc. I'm using a TeX-like theme for reminding me writing a blog like writing a public paper, although I have not actually published any paper ;)

## Author

A Mediocre Programmer from China (中华人民共和国 or P.R.C. Believe or not, there are two China in the world, one of which is trying to get rid of the name of China but not succeed yet. Wish *HER* all the best.)

I'm currently working as an Infrastructure Engineer for IBM. More specifically, I'm focusing on Linux, Openstack and Openshift.
I'm also a Developer who is experienced in building System Programs, Web Backends etc with Perl, Python and Ruby. I also know a little about C and Java.

僕は日本語もできます。2009−2011の間に日本のプロジェクトでSEを担当したことがありますので、技術関係のドキュメンタリーなどの書き読みと、日常的なコミュニケーションとかは問題ないです。余力があれば、記事の日本語版もかきたいと思います。

You can contact me via the following Email.

```bash
echo 'ZGlnZ2xpZmVAZ21haWwuY29tCg==' | base64 --decode
```

If you have no idea what's the meaning of the above mentioned code, I suppose I'm probably **NOT** the guy you want to contact.
