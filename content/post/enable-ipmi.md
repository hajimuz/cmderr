---
title: "How to enable IPMI from CLI"
date: 2020-09-23T16:50:44+08:00
draft: false
tags: ["ipmi", "openstack", "imm"]
---

I found the following error when introspecting a x3650 M5 server from Openstack Undercloud.


I tried executing `ipmitools` command directly against the server for testing.

```bash
$ ipmitool -U USERID -P 'secret' -H 192.168.24.22 users
```

And got the following error messages.
```
Error: Unable to establish LAN session
Error: Unable to establish IPMI v1.5 / RMCP session
```

Apparently it's because we are unable to establish connection to the server via **IPMI**.

It turns out that the IPMI function is disabled by default, we have to enable it manually.
For a `M5` server, we can do this on the WebUI console. But for `M4` server, it seems that we have to do it via CLI interface.


Here is the command (without error hopefully)

```bash
$ protocolcfg ipmi on
```

Normally we have to reset the IMM(or Service Processor, which is why the command is `resetsp`.)

```bash
$ resetsp
```

Wait a few minutes for the IMM to boot up. Now we should be able to send IPMI code to the server.

